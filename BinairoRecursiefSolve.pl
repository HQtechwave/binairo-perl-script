use strict;
#use warnings;

open(my $in,"<",@ARGV) or die "Couldn't open the input file!: $! \n";
our $count = 0;
our @binairo;
our $row, our $column;
my $line;
our $flag = 0;
our $iterations = 0;
my $emptyElements = 0;
while($line = <$in>){
   
   if($line =~ /<title> /){
       our @dimensions = split(" ", $line);
       ($row, $column) = split("x", $dimensions[1]);
   }
  
   if($line =~ /<g /){
      $count++;
   }

   # get the values of the binairo the save in array. 
   if(($line =~ /<text /) && ($count == 4 || $count == 5)){
      my @data = $line =~ /(\d+)/g;

      my $xCo = $data[0];
      my $yCo = $data[1];
      my $value = $data[2];

      our $yIndex = (($xCo - 20) / 40) - 1; # -1 because later use with arrays (zero based).
      our $xIndex = (($yCo - 25) / 40) - 1; # -25 because of height shift of 5 pixels in the grid.
      
      # fill the array with '.' (dots);
      if($flag == 0){ # use of flag otherwise to many iterations.
         for(my $i = 0; $i < $row; $i++){
            for(my $j = 0; $j < $column; $j++){
             $binairo[$i][$j] = ".";
            }
         }
         $flag = 1;
      }
      # overide the array with the right values.
      $binairo[$xIndex][$yIndex] = $value;
   }
}

for(my $i = 0; $i < $row; $i++){
   for(my $j = 0; $j < $column; $j++){;
         print "$binairo[$i][$j]";
   }
   print "\n";
}

print "\n";

# count empty elements in the binairo.
for(my $i = 0; $i < $row; $i++){
      for(my $j = 0; $j < $column; $j++){
         if($binairo[$i][$j] eq '.'){
            $emptyElements++;
         }
      }
}

print "There are $emptyElements empty element in the binairo.\n";

printBinairo();

sub printBinairo(){
    for(my $i = 0; $i < $row; $i++){
        for(my $j = 0; $j < $column; $j++){;
            print "$binairo[$i][$j]";
        }
        print "\n";
    }
}

sub horizontalDouble(@binairo,$i,$j){
    # horizontal doubles
               if( ((exists($binairo[$i][$j+1]) && exists($binairo[$i][$j+2])) == 1) && 
                                          ($binairo[$i][$j+1] eq $binairo[$i][$j+2]) &&
                                       (($binairo[$i][$j+1] && $binairo[$i][$j+2]) ne '.')
                  ) { 
                     # ??AA??
                     if(exists($binairo[$i][$j+3]) && ($binairo[$i][$j+3] eq '.')){
                        #?0110?
                        if($binairo[$i][$j+1] == 1){
                           $binairo[$i][$j] = 0;
                           $binairo[$i][$j+3] = 0;
                        }
                        #?1001?
                        elsif ($binairo[$i][$j+1] == 0){
                           $binairo[$i][$j] = 1;
                           $binairo[$i][$j+3] = 1;                     
                        }
                     }

                     # ..?AA
                     elsif(exists($binairo[$i][$j+3]) == 0){
                        #..011
                        if($binairo[$i][$j+1] == 1){
                           $binairo[$i][$j] = 0;
                        }
                        #..100
                        elsif ($binairo[$i][$j+1] == 0){
                           $binairo[$i][$j] = 1;
                        }
                     }
               }
}

sub verticalDouble((@binairo,$i,$j){
     #vertical doubles
               if( ((exists($binairo[$i+1][$j]) && exists($binairo[$i+2][$j])) == 1) && 
                                          ($binairo[$i+1][$j] eq $binairo[$i+2][$j]) &&
                                       (($binairo[$i+1][$j] && $binairo[$i+2][$j]) ne '.')
                  ){
                     # ??AA??
                     if(exists($binairo[$i+3][$j]) && ($binairo[$i+3][$j] eq '.')){
                        #?0110?
                        if($binairo[$i+1][$j] == 1){
                           $binairo[$i][$j] = 0;
                           $binairo[$i+3][$j] = 0;
                           verticalDouble();
                        }
                        #?1001?
                        elsif ($binairo[$i+1][$j] == 0){
                           $binairo[$i][$j] = 1;
                           $binairo[$i+3][$j] = 1;
                           verticalDouble();
                        }
                     }

                     # ..?AA
                     elsif(exists($binairo[$i+3][$j]) == 0){
                        #..011
                        if($binairo[$i+1][$j] == 1){
                           $binairo[$i][$j] = 0;
                           verticalDouble();
                        }
                        #..100
                        elsif ($binairo[$i+1][$j] == 0){
                           $binairo[$i][$j] = 1;
                           verticalDouble();
                        }
                     }
               }  
}

sub solve(){
    for(my $i = 0; $i < $row; $i++){
        for(my $j = 0; $j < $column; $j++){;
             # emtpy elements
             if($binairo[$i][$j] eq '.'){
               
                 horizontalDouble(@binairo,$i,$j);
                 solve();
                 verticalDouble(@binairo,$i,$j);
                 solve();
            }   

=c
            # 
            if( ((exists($binairo[$i][$j+1]) && exists($binairo[$i][$j+2])) == 1) && 
                                          ($binairo[$i][$j] eq $binairo[$i][$j+1]) &&
                                       (($binairo[$i][$j+2]) eq '.' && (($binairo[$i][$j] && $binairo[$i][$j+1]) ne '.'))
                  ) { 
                     if($binairo[$i][$j+1] == 1){
                           $binairo[$i][$j+2] = 0;
                     }
                     #?1001?
                     elsif ($binairo[$i][$j+1] == 0){
                           $binairo[$i][$j+2] = 1;                     
                     }
               }
=cut

        }
        print "\n";
    }
}


while($iterations < 20)
{
   # loop through the binairo and fill in the empty spots
   for(my $i = 0; $i < $row; $i++){
         for(my $j = 0; $j < $column; $j++){

            

            # emtpy elements
             if($binairo[$i][$j] eq '.'){
               # horizontal doubles
               if( ((exists($binairo[$i][$j+1]) && exists($binairo[$i][$j+2])) == 1) && 
                                          ($binairo[$i][$j+1] eq $binairo[$i][$j+2]) &&
                                       (($binairo[$i][$j+1] && $binairo[$i][$j+2]) ne '.')
                  ) { 
                     # ??AA??
                     if(exists($binairo[$i][$j+3]) && ($binairo[$i][$j+3] eq '.')){
                        #?0110?
                        if($binairo[$i][$j+1] == 1){
                           $binairo[$i][$j] = 0;
                           $binairo[$i][$j+3] = 0;
                        }
                        #?1001?
                        elsif ($binairo[$i][$j+1] == 0){
                           $binairo[$i][$j] = 1;
                           $binairo[$i][$j+3] = 1;                     
                        }
                     }

                     # ..?AA
                     elsif(exists($binairo[$i][$j+3]) == 0){
                        #..011
                        if($binairo[$i][$j+1] == 1){
                           $binairo[$i][$j] = 0;
                        }
                        #..100
                        elsif ($binairo[$i][$j+1] == 0){
                           $binairo[$i][$j] = 1;
                        }
                     }
               }

               #vertical doubles
               if( ((exists($binairo[$i+1][$j]) && exists($binairo[$i+2][$j])) == 1) && 
                                          ($binairo[$i+1][$j] eq $binairo[$i+2][$j]) &&
                                       (($binairo[$i+1][$j] && $binairo[$i+2][$j]) ne '.')
                  ){
                     # ??AA??
                     if(exists($binairo[$i+3][$j]) && ($binairo[$i+3][$j] eq '.')){
                        #?0110?
                        if($binairo[$i+1][$j] == 1){
                           $binairo[$i][$j] = 0;
                           $binairo[$i+3][$j] = 0;
                        }
                        #?1001?
                        elsif ($binairo[$i+1][$j] == 0){
                           $binairo[$i][$j] = 1;
                           $binairo[$i+3][$j] = 1;
                        }
                     }

                     # ..?AA
                     elsif(exists($binairo[$i+3][$j]) == 0){
                        #..011
                        if($binairo[$i+1][$j] == 1){
                           $binairo[$i][$j] = 0;
                        }
                        #..100
                        elsif ($binairo[$i+1][$j] == 0){
                           $binairo[$i][$j] = 1;
                        }
                     }
               }  
            }   

            # 
            if( ((exists($binairo[$i][$j+1]) && exists($binairo[$i][$j+2])) == 1) && 
                                          ($binairo[$i][$j] eq $binairo[$i][$j+1]) &&
                                       (($binairo[$i][$j+2]) eq '.' && (($binairo[$i][$j] && $binairo[$i][$j+1]) ne '.'))
                  ) { 
                     if($binairo[$i][$j+1] == 1){
                           $binairo[$i][$j+2] = 0;
                     }
                     #?1001?
                     elsif ($binairo[$i][$j+1] == 0){
                           $binairo[$i][$j+2] = 1;                     
                     }
               }



         }
   }
   $iterations++;
   print "After $iterations the binairo looks like this:\n";
   for(my $i = 0; $i < $row; $i++){
      for(my $j = 0; $j < $column; $j++){;
            print "$binairo[$i][$j]";
      }
      print "\n";
   }
   print "\n";
   print "\n";
}

print "Now there are $emptyElements emtpy elements in the binairo\n";

for(my $i = 0; $i < $row; $i++){
   for(my $j = 0; $j < $column; $j++){;
         print "$binairo[$i][$j]";
   }
   print "\n";
}

=comment
print "The latest item in the list is: \n";
for(my $i = 0; $i < 6; $i++){
   print $binairo[$i][$#binairo];

}
=cut


